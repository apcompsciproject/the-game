package hud;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import entities.Player;

public abstract class HudItem {
	private String name;
	private int value;
	public Player p;

	public HudItem(Player p) {
this.p=p;

	}




	public void setValue(int val) {
		value = val;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
	public abstract void draw(Graphics2D g);
	
	
}