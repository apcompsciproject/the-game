package states;

import dayLite.main.GameThread;
import dayLite.grid.tiles.Tile;
import dayLite.input.InputHandler;
import dayLite.input.Key;
import dayLite.states.State;
import dayLite.levels.Level;
import main.*;
import tiles.*;
import levels.HomeLand;
import levels.LevelOneScOne;
import entities.Player;
import hud.Hud;
import inventory.Inventory;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import Data.Setting;
import Data.User;

public class InGame extends State {

	private Level level;
	private Player player;
	private Key pause, debug, save;
	private InputHandler input;
	private User user = new User();
	private boolean debugging;
	@SuppressWarnings("unused")
	private boolean saving = false;
	private int ticks = 0;
	private int fps = 0;
	private int draws = 0;
	private Hud hud;

	public InGame(GameThread g, String name) {
		super(g);

		level = new Level(TheDarkSeaIslesPlatformer.GAME_SCALE,
				new Tile[] { new Grassy(), new Bush(), new Chest(), new Dirt(), new FatTree(), new Rock(), new Rocky(),
						new SkinnyTree(), new TrapStone(), new Well(), new WellTop(), new Window(), new Air(),
						new Stone(), new Grass(), new Door(), new Dirt(), new Floor(), new FatTree() },
				getGameThread());
		if (!user.fetch(name)) {
			user.setAllToZero();
			user.setName(name);
			user.setX(2);
			user.setY(2);
			user.saveAsNew();
			user.update();
		}

		level.setScale(6);
		level.setLevel(new LevelOneScOne(player = new Player(user.getX(), user.getY(), level), level));
		player.setHealth(10);
		level.setCamera(player.getCamera());
		hud = new Hud(player);
		player.setInventory(new Inventory(user.getState()));
		input = new InputHandler(TheDarkSeaIslesPlatformer.GAME.getWindow());
		input.addKey(pause = new Key(KeyEvent.VK_ESCAPE));
		input.addKey(debug = new Key(KeyEvent.VK_F3));
		input.addKey(save = new Key(KeyEvent.VK_BACK_SLASH));
		System.out.println(player.getInventory().toString());
		TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();
		// debugging = Boolean.valueOf(Setting.getSetting(3));
	}

	public void update() {
		if (pause.isPressed()) {
			// pause
			final Pause p = new Pause(TheDarkSeaIslesPlatformer.GAME, user.getName());
			new Thread(new Runnable() {
				public void run() {
					user.setX((int) player.getX());
					user.setY((int) player.getY());
					user.setState(player.getInventory().toString());
					user.save();
					p.setLoading(false);
				}
			}).start();

			TheDarkSeaIslesPlatformer.GAME.setState(p);
		}
		if (debug.isPressed() && ticks % 30 == 3) {
			List<String> f = null;
			try {
				f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/settings"),
						Charset.forName("UTF-8"));

				if (f.get(3).equals("FALSE"))
					f.set(3, "TRUE");
				else
					f.set(3, "FALSE");

				String r = "";
				for (String s : f)
					r += s + '\n';
				FileOutputStream fileOut = new FileOutputStream(
						System.getProperty("user.home") + "/.the-game/settings");
				fileOut.write(r.getBytes());
				fileOut.close();
				debugging = Boolean.valueOf(Setting.getSetting(3));

			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}
		ticks++;
		if (ticks >= 300) { // Reset ticks every 10 seconds
			save();
			ticks = 0;
		}
		if (save.isPressed())
			save();
		level.update();
		if (ticks % 20 == 0) {
			fps = draws * 3;
			draws = 0;
		}
	}

	public void save() {
		this.saving = true;
		new Thread(new Runnable() {
			public void run() {
				System.out.println("SAVING");
				user.setX((int) player.getX());
				user.setY((int) player.getY());
				user.setState(player.getInventory().toString());
				user.save();
				System.out.println("DONE");
				saving = false;
			}
		}).start();
	}

	public void render(Graphics2D g, double interpolation) {

		level.render(g, interpolation);
		if (debugging) {
			g.setColor(new Color(0, 0, 0, 128));

			g.fillRect(0, 0, getGameThread().getWidth(), 75);
			g.setColor(Color.WHITE);

			g.drawString('"' + user.getName() + "\"@" + player.getX() + "," + player.getY(), 0, 12);
			g.drawString("Save location: " + user.getX() + "," + user.getY(), 0, 26);
			g.drawString("Total ticks passed: " + ticks + ". Seconds until save: " + (int) ((300 - (ticks % 300)) / 30),
					0, 40);
			g.drawString(player.getInventory().toString(), 0, 54);
			g.drawString(String.valueOf(fps) + " FPS", 0, 68);
		}
		draws++;
		hud.draw(g);
	}

}