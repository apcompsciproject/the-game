package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Dirt extends Tile {

	public Dirt() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(7, 0, 1, 1), false);
	}

	public char id() {
		return 'd';
	}
}