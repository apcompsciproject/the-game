package entities.items.collectables;

import dayLite.grid.Grid;
import entities.items.Item;
import main.TheDarkSeaIslesPlatformer;

public class MagicDust extends Item {

	public MagicDust(float x, float y, Grid g) {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(9, 1, 1, 1), x, y, 0.5, 0.5, g);
	}

	@Override
	public int id() {
		return 0;
	}

	@Override
	public void update() {

	}
}