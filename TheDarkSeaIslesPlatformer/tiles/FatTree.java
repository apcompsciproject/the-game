package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class FatTree extends Tile {

	public FatTree() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(8, 1, 1, 1), true);
	}

	public char id() {
		return 'T';
	}
}