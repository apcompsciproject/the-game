package entities;

import java.awt.image.BufferedImage;

import dayLite.grid.Grid;
import dayLite.grid.entities.Entity;
import inventory.Inventory;

public abstract class Character extends Entity {

	public static final int INVENTORY_SIZE = 5;

	private int health;
	private double speed;

	private Inventory inventory;

	public Character(BufferedImage img, float x, float y, double w, double h, double spd, Grid g) {
		super(img, x, y, w, h, true, g);
		inventory = new Inventory(5, 5);
		speed = spd;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory i) {
		inventory = i;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int hp) {
		health = hp;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(float spd) {
		speed = spd;
	}
}