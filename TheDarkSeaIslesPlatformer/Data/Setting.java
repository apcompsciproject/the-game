package Data;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Setting {
	public static String getSetting(int lineNum) {

		List<String> f = null;
		try {
			f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/settings"),
					Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return f.get(lineNum);
	}
}