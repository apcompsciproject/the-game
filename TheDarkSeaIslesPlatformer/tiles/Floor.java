package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Floor extends Tile {

	public Floor() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(2, 2, 1, 1), false);
	}

	public char id() {
		return 'f';
	}
}