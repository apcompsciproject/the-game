package inventory;

import java.awt.image.BufferedImage;

import entities.items.Item;
import main.TheDarkSeaIslesPlatformer;

public class InventoryItem {

	public static final int WIDTH = 20, HEIGHT = 20;

	private int id, amount;
	private BufferedImage image;

	public InventoryItem(Item item) {
		id = item.id();
		image = item.getImage();
	}

	public InventoryItem(BufferedImage img, int id) {
		this.id = id;
		image = img;
	}

	public InventoryItem(String input) {
		this.id = (int) Integer.valueOf(input.substring(0, input.indexOf(';')));
		this.amount = (int) Integer.valueOf(input.substring(input.indexOf(';') + 1, input.indexOf('}')));
		switch(id){
		case 0:
			image=TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(9, 1, 1, 1);
			break;
		default:
			break;
		}
	}

	public int getID() {
		return id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amnt) {
		amount = amnt;
	}

	public BufferedImage getImage() {
		return image;
	}

	public String toString() {
		return "{" + id + ";" + amount + "}";
	}
}