package states;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import Data.Setting;
import dayLite.input.InputHandler;
import dayLite.input.Key;
import dayLite.main.GameThread;
import dayLite.states.State;
import main.TheDarkSeaIslesPlatformer;

public class Settings extends State {

	private InputHandler input;
	private Key enter;
	private String name;
	MenuList m = new MenuList();
	// TODO this should change to be a button to toggle between full screen and
	// windows mode...
	// Also, windows mode should be able to be adjusted
	MenuItem resume = new MenuItem("full", "Resume", 1);
	// TODO THIS SHOULD SAY CONTROLS, NOT QWERTY AS THE BUTTON NAME
	MenuItem controls = new MenuItem("full", "Controls", 2);
	// TODO ONCE WE SET THE BACKGROUND, THIS SHOULD NOT BE ABLE TO CHANGE
	MenuItem themeCol = new MenuItem("full", "Theme color", 3);
	// TODO Need volume controls if we want to have sounds?
	MenuItem back = new MenuItem("full", "Back", 4);

	String prior;

	public Settings(GameThread g, String p, String name) {
		super(g);
		input = new InputHandler(TheDarkSeaIslesPlatformer.GAME.getWindow());
		input.addKey(enter = new Key(KeyEvent.VK_ENTER));
		resume.addActionListener(resumeAction);
		back.addActionListener(backScrAction);

		controls.addActionListener(contr);
		themeCol.addActionListener(theme);
		m.create(resume);
		m.create(controls);
		m.create(themeCol);
		m.create(back);

		prior = p;
		resume.setDisabled(false);
		back.setDisabled(false);

		controls.setDisabled(false);
		themeCol.setDisabled(false);
		this.name = name;
	}

	@SuppressWarnings("serial")
	Action contr = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {

			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();

			TheDarkSeaIslesPlatformer.GAME.setState(new Controls(TheDarkSeaIslesPlatformer.GAME, prior));
		}
	};
	@SuppressWarnings("serial")
	Action theme = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			List<String> f = null;
			ArrayList<Color> c = new ArrayList<Color>();
			c.add(Color.BLACK);
			c.add(Color.green);
			c.add(Color.BLUE);
			c.add(Color.CYAN);
			c.add(Color.MAGENTA);
			try {
				f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/settings"),
						Charset.forName("UTF-8"));
				try {
					f.set(2, Integer.toString((c.get(c.indexOf(new Color(Integer.parseInt(f.get(2)))) + 1)).getRGB()));
				} catch (IndexOutOfBoundsException e1) {
					f.set(2, Integer.toString((c.get(0).getRGB())));
				}
				String r = "";
				for (String s : f)
					r += s + '\n';
				FileOutputStream fileOut = new FileOutputStream(
						System.getProperty("user.home") + "/.the-game/settings");
				fileOut.write(r.getBytes());
				fileOut.close();

			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
	};
	@SuppressWarnings("serial")
	Action resumeAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			resume();
		}
	};

	@SuppressWarnings("serial")
	Action backScrAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();

			switch (prior) {
			case "pause":
				Pause pause = new Pause(TheDarkSeaIslesPlatformer.GAME, name);
				pause.setLoading(false);
				TheDarkSeaIslesPlatformer.GAME.setState(pause);
				break;
			case "mainMenu":
				TheDarkSeaIslesPlatformer.GAME.setState(new MainMenu(TheDarkSeaIslesPlatformer.GAME));
				break;
			default:
				// TODO Create an error screen state to send us to catch
				// mistakes
				TheDarkSeaIslesPlatformer.GAME.setState(new MainMenu(TheDarkSeaIslesPlatformer.GAME)); // TODO
																										// change
																										// to
																										// error
																										// state
				break;
			}
		}
	};

	public void update() {
		if (enter.isPressed()) {
			resume();
		}
	}

	public void resume() {
		m.removeList();
		TheDarkSeaIslesPlatformer.GAME.setState(new InGame(TheDarkSeaIslesPlatformer.GAME, name));
	}

	public void render(Graphics2D g, double interpolation) {
		g.setColor(new Color(Integer.parseInt(Setting.getSetting(2))));
		g.fillRect(0, 0, 1000, 1000);
		g.setColor(Color.RED);
		try {
			Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(80f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			// register the font
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
			g.setFont(customFont);
		} catch (IOException | FontFormatException e) {
			e.printStackTrace();
			g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
		}
		g.drawString("Settings", 310, 55);
		g.setColor(Color.gray);
		m.paint(g);
	}
}