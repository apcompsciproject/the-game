package main;

import states.*;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import Data.Setting;
import dayLite.main.GameThread;
import dayLite.graphics.SpriteSheet;
import dayLite.graphics.ImageProcessor;

public class TheDarkSeaIslesPlatformer {

	// Game Constants
	public static final int LEVEL_WIDTH = 25, LEVEL_HEIGHT = 15, GAME_SCALE = 16;
	public static final int CONFIG_VERSION = 2;
	public static int TPS = 30, DPS = 120;

	// Sprite Sheet(s)

	/**
	 * IF game does not run, try: adding 'TheDarkSeaIslesPlatformer/' to (or
	 * remove it from) the start of string inside the loadImage() method call
	 * below.
	 * 
	 * This issue arises depending on what folder you chose as the root for the
	 * project. Root should be: TheDarkSeaIslesPlatformer Root should NOT be:
	 * The parent folder of TheDarkSeaIslesPlatformer
	 */
	public static SpriteSheet TOWN_SPRITES = new SpriteSheet(

			/**
			 * IF game does not run, try: adding 'TheDarkSeaIslesPlatformer/' to
			 * (or remove it from) the start of string inside the loadImage()
			 * method call below.
			 * 
			 * This issue arises depending on what folder you chose as the root
			 * for the project. Root should be: TheDarkSeaIslesPlatformer Root
			 * should NOT be: The parent folder of TheDarkSeaIslesPlatformer
			 */
			ImageProcessor.loadImage("sprites/town_tiles.png", false), 16);
	public static final GameThread GAME = new GameThread(800, 600, 800, 600, "The Dark Sea Isles Platformer", true);

	public static void main(String[] args) {
		if (System.getProperty("os.name").toLowerCase().contains("windows"))
			TOWN_SPRITES = new SpriteSheet(ImageProcessor.loadImage("sprites/town_tiles.png", false), 16);
		if (!(new File(System.getProperty("user.home") + "/.the-game").isDirectory())) {
			new File(System.getProperty("user.home") + "/.the-game").mkdirs();
			try {
				File f = new File(System.getProperty("user.home") + "/.the-game/settings");
				f.createNewFile();
				FileWriter fw = null;
				try {

					fw = new FileWriter(System.getProperty("user.home") + "/.the-game/settings", true);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				try {
					fw.write("0");

					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		createSettings();

		GAME.setState(new MainMenu(GAME));
		GAME.setPerformanceOut(true);
		GAME.setRefreshColor(Color.BLACK);
		// GAME.setFullscreen(true);
		GAME.init(TPS, DPS);
	}

	public static void createSettings() {

		String[][] lines = { { "0", "2" }, // CONFIGVERSION
				{ "1", "QWERTY" }, // kb layout
				{ "2", "" + Color.BLACK.getRGB() }, // theme color
				{ "2", "TRUE" } // show debug?
		};
		int workingversion = Integer.valueOf(Setting.getSetting(0));

		List<String> fl = null;
		try {
			fl = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/settings"),
					Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayList<String> working = new ArrayList<String>();
		working.addAll(fl);
		for (String[] s : lines) {
			if (Integer.valueOf(s[0]) > workingversion)
				working.add(s[1]);
		}
		working.set(0, Integer.valueOf(CONFIG_VERSION).toString());

		try {
			String r = "";
			for (String s : working)
				r += s + '\n';
			FileOutputStream fileOut = new FileOutputStream(System.getProperty("user.home") + "/.the-game/settings");
			fileOut.write(r.getBytes());
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}