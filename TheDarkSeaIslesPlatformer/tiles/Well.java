package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

 public class Well extends Tile {

	public Well() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(5, 1, 1, 1), true);
	}

	@Override
	public char id() {
		return 'W';
	}
}