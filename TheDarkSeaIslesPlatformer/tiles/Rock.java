package tiles;

import dayLite.grid.tiles.LogicTile;
import main.TheDarkSeaIslesPlatformer;
import status.actions.Action;
import status.actions.Interactable;

public class Rock extends LogicTile implements Interactable {

	public Rock() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(5, 3, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), true);
	}

	@Override
	public char id() {
		return 'R';
	}

	@Override
	public void update() {
	}

	@Override
	public void use(Action actn) {
		if (actn.getName().equals("Punch")) {
			getGrid().setTile(getX(), getY(), new Dirt());
			this.remove();
		}
	}

}
