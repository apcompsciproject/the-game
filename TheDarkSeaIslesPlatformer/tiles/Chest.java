package tiles;

import dayLite.grid.tiles.LogicTile;
import entities.items.collectables.MagicDust;
import main.TheDarkSeaIslesPlatformer;
import status.actions.Action;
import status.actions.Interactable;

public class Chest extends LogicTile implements Interactable {

	public Chest() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(3, 1, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(2, 2, 1, 1), true);
	}

	@Override
	public void update() {

	}

	@Override
	public char id() {
		return 'C';
	}

	@Override
	public void use(Action actn) {
		if (actn.getName().equals("Use")) {
			MagicDust dust = new MagicDust(getX() + 1, getY(), getGrid());
			getGrid().addEntity(dust);
		} 
	}
}