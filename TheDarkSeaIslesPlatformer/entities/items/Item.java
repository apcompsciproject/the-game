package entities.items;

import entities.Player;
import inventory.InventoryItem;

import java.awt.image.BufferedImage;

import dayLite.grid.Grid;
import dayLite.grid.entities.Entity;

public abstract class Item extends Entity {

	public Item(BufferedImage img, float x, float y, double w, double h, Grid g) {
		super(img, x, y, w, h, false, g);
	}

	@Override
	public void todo() {
		super.todo();
		Entity hit = this.getLastEntityHit();
		if (hit != null) {
			if (hit instanceof Player) {
				((Player) hit).getInventory().add(new InventoryItem(this), 1);
				this.removeFromGrid();
			}
		}
	}

	public abstract int id();
}