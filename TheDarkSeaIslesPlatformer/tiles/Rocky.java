package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Rocky extends Tile {

	public Rocky() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(7, 3, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), false);
	}

	@Override
	public char id() {
		return 'r';
	}
}