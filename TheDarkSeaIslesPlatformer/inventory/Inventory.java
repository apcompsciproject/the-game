package inventory;

import java.util.Arrays;

public class Inventory {
	private int numRows, numCols;
	private InventoryItem[][] contents;

	public Inventory(int rows, int cols) {
		contents = new InventoryItem[rows][cols];
		numRows = rows;
		numCols = cols;
	}

	public Inventory(String input) {

		numRows = 5;
		numCols = 5;
		contents = new InventoryItem[numRows][numCols];
		String[] e = input.split("\\{");
		int index = 0;
		for (String s : e) {
			if (s.matches("^[0-9].+")) {
				contents[index / 5][index % 5] = new InventoryItem(s);
				index++;
			}
		}
	}

	public void add(InventoryItem item, int num) {
		int id = item.getID(), openRow = -1, openCol = -1;
		for (int row = 0; row < numRows; row++) {
			for (int col = 0; col < numCols; col++) {
				InventoryItem current = contents[row][col];

				if (openRow == -1 && current == null) {
					openRow = row;
					openCol = col;
				}

				else if (current != null && current.getID() == id) {
					current.setAmount(current.getAmount() + num);
					return;
				}
			}
		}

		if (openRow != -1) {
			item.setAmount(num);
			contents[openRow][openCol] = item;
		}

	}

	public InventoryItem remove(int row, int col) {
		InventoryItem tmp = contents[row][col];
		contents[row][col] = null;
		return tmp;
	}

	public void clear() {
		for (int row = 0; row < numRows; row++) {
			for (int col = 0; col < numCols; col++) {
				contents[row][col] = null;
			}
		}
	}

	public InventoryItem get(int row, int col) {
		return contents[row][col];
	}

	public void set(int row, int col, InventoryItem item) {
		contents[row][col] = item;
	}

	public int getRows() {
		return numRows;
	}

	public int getCols() {
		return numCols;
	}

	public String toString() {
		return Arrays.deepToString(contents);
	}
}