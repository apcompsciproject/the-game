package hud;

import java.util.ArrayList;

import dayLite.graphics.ImageProcessor;
import entities.Player;

import java.awt.Graphics2D;

public class Hud {
	private Bar bar;
	private Inventory i;
	private ArrayList<HudItem> items;
	private Player player;

	public Hud(Player p) {
player=p;
bar=new Bar(p);
i=new Inventory(p);
	}


	public void addItem(HudItem itm) {
		// add to Array list of items
		items.add(itm);
		// draw
	}
	public void draw(Graphics2D g){
		//for(HudItem h:items)
			//h.draw(g);
		bar.draw(g);
		i.draw(g);
	}

}