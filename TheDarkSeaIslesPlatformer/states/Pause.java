package states;

import main.TheDarkSeaIslesPlatformer;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import Data.Setting;
import dayLite.input.InputHandler;
import dayLite.states.State;
import dayLite.main.GameThread;
import dayLite.input.Key;

public class Pause extends State {

	private InputHandler input;
	private Key enter;
	private String name;
	MenuList m = new MenuList();
	MenuItem resume = new MenuItem("full", "Resume", 1);
	MenuItem loadG = new MenuItem("full", "Load Game", 2);
	MenuItem set = new MenuItem("full", "Settings", 3);
	MenuItem mainScr = new MenuItem("full", "Exit to Main Menu", 4);
	MenuItem quit = new MenuItem("full", "Exit to Windows", 5);

	private boolean loading = true;

	public Pause(GameThread g, String name) {
		super(g);
		input = new InputHandler(TheDarkSeaIslesPlatformer.GAME.getWindow());
		input.addKey(enter = new Key(KeyEvent.VK_ENTER));
		resume.addActionListener(resumeAction);
		loadG.addActionListener(loadAction);
		quit.addActionListener(quitAction);
		m.create(resume);
		m.create(set);
		set.addActionListener(settings);
		m.create(loadG);
		m.create(mainScr);
		mainScr.addActionListener(mainScrAction);
		m.create(quit);
		resume.setDisabled(true);
		quit.setDisabled(true);
		set.setDisabled(true);
		loadG.setDisabled(true);
		this.name = name;
	}

	public boolean isLoading() {
		return loading;
	}

	public void setLoading(boolean loading) {
		this.loading = loading;
		resume.setDisabled(false);
		quit.setDisabled(false);
		set.setDisabled(false);
		loadG.setDisabled(false);
	}

	@SuppressWarnings("serial")
	Action resumeAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			resume();

		}
	};

	@SuppressWarnings("serial")
	Action loadAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!loading) {
				loading = true;
				m.removeList();
				TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();

				TheDarkSeaIslesPlatformer.GAME
						.setState(new ConfirmNavigation(TheDarkSeaIslesPlatformer.GAME, "pause", "name"));
			}
		}
	};

	@SuppressWarnings("serial")
	Action quitAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.setState(new ConfirmNavigation(TheDarkSeaIslesPlatformer.GAME, "pause", ""));
		}
	};
	@SuppressWarnings("serial")
	Action settings = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.setState(new Settings(TheDarkSeaIslesPlatformer.GAME, "pause", name));
		}
	};
	@SuppressWarnings("serial")
	Action mainScrAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.setState(new MainMenu(TheDarkSeaIslesPlatformer.GAME));
		}
	};

	public void update() {
		if (enter.isPressed()) {
			resume();
		}
	}

	public void resume() {
		m.removeList();
		TheDarkSeaIslesPlatformer.GAME.setState(new InGame(TheDarkSeaIslesPlatformer.GAME, name));
	}

	public void render(Graphics2D g, double interpolation) {
		g.setColor(new Color(Integer.parseInt(Setting.getSetting(2))));
		g.fillRect(0, 0, 1000, 1000);
		g.setColor(Color.RED);
		if (loading)
			g.drawString("Saving...", 25, 80);
		else {
			try {
				Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(120f);
				GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
				// register the font
				ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
				g.setFont(customFont);
			} catch (IOException | FontFormatException e) {
				e.printStackTrace();
				g.setFont(new Font("MS Gothic", Font.PLAIN, 100));
			}
			g.drawString("Game Paused", 210, 100);

		}

		g.setColor(Color.gray);
		m.paint(g);
	}
}