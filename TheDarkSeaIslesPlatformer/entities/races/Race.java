package entities.races;

public interface Race {

	public abstract int initialHealth();

	public abstract int initialAttack();

	public abstract int initialStamina();

	public abstract int initialDefense();

	public abstract String getName();
}