package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Grassy extends Tile {

	public Grassy() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(2, 3, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), false);
	}

	@Override
	public char id() {
		return ',';
	}
}