package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class WellTop extends Tile {

	public WellTop() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(4, 1, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), false);
		setOnTop(true);
	}

	@Override
	public char id() {
		return 'v';
	}
}