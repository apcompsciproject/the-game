package states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;

import main.TheDarkSeaIslesPlatformer;

@SuppressWarnings("serial")
public class MenuItem extends JButton {
	private String type = "";
	private String text = "";
	private int level;
	public static final String FULL = "full";
	public static final String LEFT = "left";
	public static final String RIGHT = "right";

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	private boolean disabled = false;

	public MenuItem(String place, String name, int level) {
		type = place;
		text = name;
		this.level = level;

	}

	public void setBounds() {
		if (type.equals("full"))
			setBounds(TheDarkSeaIslesPlatformer.GAME.getWidth() / 4, (int) ((100 + (level * 50)) * 1),
					TheDarkSeaIslesPlatformer.GAME.getWidth() / 2, 30);
		if (type.equals("left"))
			setBounds(TheDarkSeaIslesPlatformer.GAME.getWidth() / 4, (int) ((100 + (level * 50)) * 1),
					TheDarkSeaIslesPlatformer.GAME.getWidth() / 6, 30);
		if (type.equals("right"))
			setBounds(TheDarkSeaIslesPlatformer.GAME.getWidth() * 7 / 12, (int) ((100 + (level * 50)) * 1),
					TheDarkSeaIslesPlatformer.GAME.getWidth() / 6, 30);
	}

	public void paint(Graphics g) {
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		if (disabled)
			g.setColor(Color.DARK_GRAY);
		else

			g.setColor(Color.GRAY);

		if (type.equals("full")) {
			g.fillRect(((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 4, 100 + (level * 50),
					((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 2, 30);
			g.setColor(Color.WHITE);
			g.drawString(text, ((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 4,
					(int) (100 + (level * 50) + (37.5 / 2 + 6)));
		}
		if (type.equals("left")) {
			g.fillRect(((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 4, 100 + (level * 50),
					((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 6, 30);
			g.setColor(Color.WHITE);
			g.drawString(text, ((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 4,
					(int) (100 + (level * 50) + (37.5 / 2 + 6)));
		}
		if (type.equals("right")) {
			g.fillRect(((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) * 7 / 12, 100 + (level * 50),
					((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) / 6, 30);
			g.setColor(Color.WHITE);
			g.drawString(text, ((int) TheDarkSeaIslesPlatformer.GAME.getXResolution()) * 7 / 12,
					(int) (100 + (level * 50) + (37.5 / 2 + 6)));
		}

	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		setEnabled(!disabled);
	}
}
