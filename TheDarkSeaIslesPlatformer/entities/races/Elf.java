package entities.races;

import java.awt.image.BufferedImage;

import dayLite.grid.Grid;
import entities.Character;

public class Elf extends Character implements Race {

	public Elf(BufferedImage img, float x, float y, int w, int h, Grid g) {
		super(img, x, y, w, h, 50, g);
	}

	@Override
	public int initialHealth() {
		return 0;
	}

	@Override
	public int initialAttack() {
		return 0;
	}

	@Override
	public int initialStamina() {
		return 0;
	}

	@Override
	public int initialDefense() {
		return 0;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void update() {
	}
}