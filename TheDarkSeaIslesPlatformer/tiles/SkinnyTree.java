package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class SkinnyTree extends Tile {

	public SkinnyTree() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(6, 1, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), false);
		setOnTop(true);
	}

	@Override
	public char id() {
		return 't';
	}
}