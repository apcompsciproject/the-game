package states;

import main.TheDarkSeaIslesPlatformer;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import Data.Setting;
import dayLite.input.InputHandler;
import dayLite.input.Key;
import dayLite.states.State;
import dayLite.main.GameThread;

public class MainMenu extends State {

	private InputHandler input;
	private Key enter;

	MenuList m = new MenuList();
	MenuItem resume = new MenuItem("left", "Start", 2);
	MenuItem set = new MenuItem("right", "Settings", 2);

	MenuItem ld = new MenuItem("left", "Load Game", 4);
	MenuItem quit = new MenuItem("right", "Quit", 4);

	private int ticks;
	private boolean loading = true;

	public MainMenu(GameThread g)

	{
		super(g);

		input = new InputHandler(TheDarkSeaIslesPlatformer.GAME.getWindow());
		input.addKey(enter = new Key(KeyEvent.VK_ENTER));

		System.out.println("Finished Constructor");
	}

	@SuppressWarnings("serial")
	Action resumeAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!loading) {
				loading = true;
				m.removeList();
				TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();

				TheDarkSeaIslesPlatformer.GAME.setState(new InGame(TheDarkSeaIslesPlatformer.GAME, ""));
			}
		}
	};

	@SuppressWarnings("serial")
	Action loadAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!loading) {
				loading = true;
				m.removeList();
				TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();

				TheDarkSeaIslesPlatformer.GAME
						.setState(new ConfirmNavigation(TheDarkSeaIslesPlatformer.GAME, "mainMenu", "name"));
			}
		}
	};

	@SuppressWarnings("serial")
	Action settings = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!loading) {
				loading = true;
				m.removeList();
				TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();

				TheDarkSeaIslesPlatformer.GAME.setState(new Settings(TheDarkSeaIslesPlatformer.GAME, "mainMenu", ""));
			}
		}
	};
	@SuppressWarnings("serial")
	Action quitAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME
					.setState(new ConfirmNavigation(TheDarkSeaIslesPlatformer.GAME, "mainMenu", ""));
		}
	};

	public void render(Graphics2D g, double interpolation) {
		//g.setColor(new Color(Integer.parseInt(Setting.getSetting(2))));
		g.fillRect(0, 0, 1000, 1000);
		g.setColor(Color.WHITE);

		if (loading) {
			g.setFont(new Font("Purisa", Font.PLAIN, 10));
			g.drawString("Loading...", 550, 450);
			try {
				Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(68f);
				GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
				// register the font
				ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
				g.setFont(customFont);
			} catch (IOException | FontFormatException e) {
				e.printStackTrace();
				g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
			}
			g.drawString("Welcome to", 300, 50);

			try {
				Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(136f);
				GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
				// register the font
				ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
				g.setFont(customFont);
			} catch (IOException | FontFormatException e) {
				e.printStackTrace();
				g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
			}
			g.drawString("The Dark Sea", 150, 150);
		} else {
			try {
				Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(68f);
				GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
				// register the font
				ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
				g.setFont(customFont);
			} catch (IOException | FontFormatException e) {
				e.printStackTrace();
				g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
			}
			g.drawString("Welcome to", 300, 50);

			try {
				Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(136f);
				GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
				// register the font
				ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
				g.setFont(customFont);
			} catch (IOException | FontFormatException e) {
				e.printStackTrace();
				g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
			}
			g.drawString("The Dark Sea", 150, 150);
		}
		m.paint(g);

	}

	@Override
	public void update() {
		if (loading) {
			ticks++;
		}
		if (ticks == 10) {
			resume.addActionListener(resumeAction);
			quit.addActionListener(quitAction);
			set.addActionListener(settings);
			ld.addActionListener(loadAction);

			m.create(resume);
			m.create(set);
			m.create(ld);
			m.create(quit);
			resume.setDisabled(false);
			quit.setDisabled(false);
			set.setDisabled(false);
			ld.setDisabled(false);
			loading = false;
		}
		if (enter.isPressed() && !loading) {
			loading = true;
			resume.setDisabled(true);
			quit.setDisabled(true);
			set.setDisabled(true);
			ld.setDisabled(true);
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();

			TheDarkSeaIslesPlatformer.GAME.setState(new InGame(TheDarkSeaIslesPlatformer.GAME, ""));
		}
	}
}