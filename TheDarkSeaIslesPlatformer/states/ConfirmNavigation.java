package states;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import Data.Setting;
import dayLite.main.GameThread;
import dayLite.states.State;
import main.TheDarkSeaIslesPlatformer;

public class ConfirmNavigation extends State{
	MenuList m = new MenuList();
	String prior;
	String name;
	MenuItem back = new MenuItem("right", "No", 3);
	MenuItem confirm = new MenuItem("left", "Yes", 3);
	
	
	
	
	public ConfirmNavigation(GameThread g, String p, String n) {
		super(g);
		name = n;
		prior = p;
		back.addActionListener(backScrAction);
		confirm.addActionListener(confirmAction);
		
		m.create(back);
		m.create(confirm);
		back.setDisabled(false);
		
		confirm.setDisabled(false);
		
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
	
	@SuppressWarnings("serial")
	Action backScrAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			

			switch(prior){
			case "pause":
				Pause pause= new Pause(TheDarkSeaIslesPlatformer.GAME, name);
				pause.setLoading(false);
				TheDarkSeaIslesPlatformer.GAME.setState(pause);
				break;
			case "mainMenu":
				
				TheDarkSeaIslesPlatformer.GAME.setState(new MainMenu(TheDarkSeaIslesPlatformer.GAME));
				break;
			default:
				// TODO Create an error screen state to send us to catch mistakes 
				TheDarkSeaIslesPlatformer.GAME.setState(new MainMenu(TheDarkSeaIslesPlatformer.GAME)); // TODO change to error state 
				break;
			}
		}
	};
	
	@SuppressWarnings("serial")
	Action confirmAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			
			if(name == ""){
				TheDarkSeaIslesPlatformer.GAME.getWindow().dispose();
			} else {
			TheDarkSeaIslesPlatformer.GAME.getWindow().requestFocus();
				

			TheDarkSeaIslesPlatformer.GAME.setState(new InGame(TheDarkSeaIslesPlatformer.GAME, ""));
			}
		}
	};

	
	
	
	@Override
	public void render(Graphics2D g, double interpolation) {
		// TODO Auto-generated method stub
		g.setColor(new Color( Integer.parseInt(Setting.getSetting(2))));
		g.fillRect(0, 0, 1000, 1000);
		g.setColor(Color.RED);
		try {
			Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(150f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			//register the font
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
			g.setFont(customFont);
		} catch (IOException | FontFormatException e) {
			e.printStackTrace();
			g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
		}
		g.drawString("Confirm?", 230, 130);
		g.setColor(Color.gray);
		m.paint(g);
	}

	@Override
	public void update() {
	}
}
