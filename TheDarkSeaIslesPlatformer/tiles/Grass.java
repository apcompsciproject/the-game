package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Grass extends Tile {

	public Grass() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), false);
	}

	public char id() {
		return 'g';
	}
}