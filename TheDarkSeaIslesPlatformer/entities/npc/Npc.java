package entities.npc;

import java.awt.image.BufferedImage;

import dayLite.grid.Grid;
import entities.Character;

public abstract class Npc extends Character {

	public Npc(BufferedImage img, float x, float y, int w, int h, float spd, Grid g) {
		super(img, x, y, w, h, spd, g);
	}

	@Override
	public void todo() {
		super.todo();
	}
}