package tiles;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Bush extends Tile {

	public Bush() {
		super(TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 3, 1, 1),
				TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 1, 1, 1), false);
		setOnTop(true);
	}

	@Override
	public char id() {
		return 'B';
	}
}