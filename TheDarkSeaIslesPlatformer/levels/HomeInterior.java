 package levels;

import java.util.ArrayList;

import entities.*;
import dayLite.grid.entities.Entity;
import dayLite.levels.Level;
import dayLite.levels.LevelData;

public class HomeInterior implements LevelData {

	private char[][] tiles;
	private ArrayList<Entity> entities;

	private Player player;
	private Level level;

	public HomeInterior(Player p, Level lvl) {
		tiles = new char[][] {
				{ 'S', 'S', 'S', 'S', 'S', 'S'},
				{ 'S', 'C', 'C', 'C', 'C', 'S'},
				{ 'S', 'f', 'f', 'f', 'f', 'S'},
				{ 'S', 'f', 'f', 'f', 'f', 'S'},
				{ 'S', 'f', 'f', 'f', 'f', 'S'},
				{ 'S', 'f', 'f', 'f', 'f', 'S'},
				{ 'S', 'D', 'S', 'S', 'S', 'S'}};

	    level = lvl;
		entities = new ArrayList<>();
		entities.add(p);
		player = p;
	}

	public char[][] tileMemory() {
		return tiles;
	}

	public ArrayList<Entity> entityMemory() {
		return entities;
	}

	public Player getPlayer() {
		return player;
	}

	public void update() {
		if (player.getX() >= 1 && player.getX() <= 2 && player.getY() >= 6.50) {
			player.setPosition(3, 1);
			level.setLevel(new HomeLand(player, level));
		}
	}
}