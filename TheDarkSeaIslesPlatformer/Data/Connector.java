package Data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Connector {


	/**
	 * Create the Connector No Paramaters, these are hand coded in.
	 */
	public Connector() {
				if(!(new File(System.getProperty("user.home") + "/.the-game/save.dat").exists())){
					try {
				File f = new File(System.getProperty("user.home") + "/.the-game/save.dat");
				f.createNewFile();

			} catch (IOException e) {
				e.printStackTrace();
			}}

	}

	/**
	 * @returns Cell requested
	 * @param id:
	 *            What condition tests against, (ie WHERE id=condtion)
	 * @param condition:
	 *            What id is tested for (ex WHERE name='bob')
	 * @param col:
	 *            The column that you want to know (ex health)
	 * @deprecated
	 */
	public Object getCell(String table, String id, String condition, String col) {
			List<String> f = null;
		try {
			f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/save.dat"),
					Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(String s : f)
		if(s.startsWith(col.toLowerCase()))
		return s.substring(s.indexOf('=')+1);
		return null;
	}
		/**
	 * @returns Cell requested
	 * 
	 */
	public String getValue(String key) {
			List<String> f = null;
		try {
			f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/save.dat"),
					Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(String s : f)
		if(s.startsWith(key.toLowerCase()))
		return s.substring(s.indexOf('=')+1);
		return null;
	}

	/**
	 * @param table:
	 *            The table (ex Users) that you need to set a cell in.
	 * @param id:
	 *            What condition tests against, (ie WHERE id=condtion)
	 * @param condition:
	 *            What id is tested for (ex WHERE name='bob')
	 * @param col:
	 *            The column that you need to change (ex health)
	 * @param newData:
	 *            What the cell's new value will be (ex 20)
	 * @deprecated
	 */
	public boolean setCell(String table, String id, String condition, String col, String newData) {
	List<String> f = null;
			try {
				f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/save.dat"),
						Charset.forName("UTF-8"));
						for(String s: f)
						if(s.startsWith(col.toLowerCase()))
						f.set(f.indexOf(s), newData);


				
				String r = "";
				for (String s : f)
					r += s + '\n';
				FileOutputStream fileOut = new FileOutputStream(
						System.getProperty("user.home") + "/.the-game/save.dat");
				fileOut.write(r.getBytes());
				fileOut.close();

			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				return false;
			}
		return true;
	}
		public boolean setValue(String key, String newData) {
	List<String> f = null;
			try {
				f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/save.dat"),
						Charset.forName("UTF-8"));
						for(String s: f)
						if(s.startsWith(key.toLowerCase()))
						f.set(f.indexOf(s), key+"="+newData);


				
				String r = "";
				for (String s : f)
					r += s + '\n';
				FileOutputStream fileOut = new FileOutputStream(
						System.getProperty("user.home") + "/.the-game/save.dat");
				fileOut.write(r.getBytes());
				fileOut.close();

			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				return false;
			}
			return true;}
}
