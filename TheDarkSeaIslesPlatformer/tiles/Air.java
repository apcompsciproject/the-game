package tiles;

import dayLite.grid.tiles.Tile;

public class Air extends Tile {

	public Air() {
		super(null, true);
	}

	public char id() {
		return 'a';
	}
}
