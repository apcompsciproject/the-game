package status.actions;

public class Action {

	private String name;
	private double value;

	public Action(String nm, double val) {
		name = nm;
		value = val;
	}

	public String getName() {
		return name;
	}

	public double getValue() {
		return value;
	}
}