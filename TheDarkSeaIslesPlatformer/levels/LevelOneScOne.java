 package levels;

import java.util.ArrayList;

import entities.*;
import entities.items.collectables.MagicDust;
import dayLite.grid.entities.Entity;
import dayLite.levels.Level;
import dayLite.levels.LevelData;

public class LevelOneScOne implements LevelData {

	private char[][] tiles;
	private ArrayList<Entity> entities;

	private Level level;
	private Player player;

	public LevelOneScOne(Player p, Level lvl) {
		tiles = new char[][] {
				{ 'T', 'T', 'T', 'r', 't', 't', 't', 'g', ',', 'g', 't', 't', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T' },
				{ 'd', 'd', 'd', 'T', 'T', 'T', 'T', 'g', ',', 'g', 'T', 'g', 'g', 'T', 'g', ',', 'g', ',', 'T', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'g', 'T', 'd', 'T', 'g', 'g', 'T', ',', 'g', ',', 'T', ',', 'g', 'r', 'd', 'd', 'd', 'd', 'd', 'T', 'g', 'r', ',', 'g', 'T' },
				{ 'T', 'T', 'd', 'T', 'g', ',', 'T', 'T', 'd', 'T', 'T', ',', 'g', 'T', 'T', 'T', 't', 'T', 'd', 'T', 'T', ',', 'g', ',', 'T' },
				{ 't', 't', 'd', 'T', 't', 't', ',', 'T', 'd', 'T', ',', 'g', 'g', 'T', 'g', 'g', 'g', 'T', 'd', 'd', 'd', 'T', ',', 'g', 'T' },
				{ 'T', 'T', 'd', 'd', 'T', 'g', 'g', 'T', 'd', 'T', 'g', ',', ',', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'd', 'd', 'T', 'g', 'T' },
				{ 'T', 't', 'T', 'd', 'T', ',', 'T', 'd', 'd', 'T', 'g', 'g', 'g', 't', 't', 't', 't', 't', 't', 't', 'T', 'd', 'T', ',', 'T' },
				{ 'T', ',', 'T', 'd', 'T', 'g', 'T', 'd', 'T', 'g', 'g', 'g', ',', 'T', 'T', 't', 'r', ',', 'g', 't', 'T', 'd', 'T', 'g', 'T' },
				{ 'T', 't', 'd', 'd', 'T', 'g', 'T', 'd', 'T', 'g', ',', 'r', 'g', 'T', 'd', 'd', 'd', 'T', 'd', 'd', 'd', 'd', 'T', 'g', 'T' },
				{ 'T', 'T', 'd', 'T', 'T', 'T', 'd', 'd', 'T', ',', 'g', 'g', ',', 'T', 'd', 't', 'g', ',', 'r', 't', 'T', 'd', 'T', ',', 'T' },
				{ 'T', 'd', 'd', 'd', 'd', 'd', 'd', 'T', ',', 'g', 'g', ',', 'g', 'T', 'd', 't', 't', 't', 't', 't', 'T', 'd', 'T', 'g', 'T' },
				{ 'T', 'd', 'T', 'T', 'd', 'T', 'T', 'g', 'g', ',', 'g', 'g', 'T', 'd', 'd', 'T', 'T', 'T', 'T', 'T', 'T', 'd', 'T', ',', 'T' },
				{ 'T', 'd', 'r', 'T', 'd', 'T', ',', ',', 'C', ',', 'g', 'T', 'T', 'd', 'T', 'g', ',', 'g', ',', 'T', 'd', 'd', 'T', 'g', 'T' },
				{ 'T', 'd', 'g', 'T', 'd', 'T', ',', 'g', ',', 'g', 'T', 'd', 'd', 'd', 'T', 'g', 'g', ',', ',', 'T', 'd', 'T', ',', 'g', 'T' },
				{ 'T', 'd', ',', 'T', 'd', 'T', 'T', 'T', 'g', 'T', 'd', 'd', 'T', 'T', 't', 'T', 't', 't', 'g', 'T', 'd', 'T', 'g', ',', 'T' },
				{ 'T', 'd', 'r', 'T', 'd', 'd', 'd', 'd', 'T', 'r', 'd', 'T', 'g', 'g', 'T', 'g', ',', 'T', 'T', 'T', 'd', ',', 'T', 'g', 'T' },
				{ 'T', 'd', 'T', ',', 'T', 'T', 'T', 'd', 'T', 'd', 'd', 'T', ',', 'g', 'T', ',', 'g', 'r', 'd', 'd', 'd', 'd', 'd', ',', 'T' },
				{ 'T', 'd', 'T', 'g', 'g', 'g', 'T', 'd', 'd', 'd', 'T', 'g', 'g', ',', 't', 'g', 'g', 'T', 'T', 'T', 'T', 'T', 'd', ',', 'T' },
				{ 'T', 'C', 'T', 'C', 'g', 'g', 't', 't', ',', 'g', 'T', 'C', 'g', ',', 'T', 'T', 't', 't', ',', 'g', 'C', 'T', 'd', 'g', 'T' },
				{ 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'd', 'T', 'T' } };

		level = lvl;
		entities = new ArrayList<>();
		entities.add(p);
		player = p;

		entities.add(new MagicDust(2, 2, null));
	}

	public char[][] tileMemory() {
		return tiles;
	}

	public ArrayList<Entity> entityMemory() {
		return entities;
	}

	public Player getPlayer() {
		return player;
	}

	public void update() {
		if (player.getX() >= 3 && player.getX() <= 4 && player.getY() <= 0.25) {
			player.setPosition(1, 5);
			level.setLevel(new HomeInterior(player, level));
		} else if(player.getX()>= 22 && player.getX() <= 23 && player.getY() >= 18.75){
			player.setPosition(23, 1);
			level.setLevel(new HomeLand(player, level));
		}
	}
}