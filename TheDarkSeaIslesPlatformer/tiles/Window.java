package tiles;

import java.awt.image.BufferedImage;

import dayLite.grid.tiles.LogicTile;
import main.TheDarkSeaIslesPlatformer;
import status.actions.Action;
import status.actions.Interactable;

public class Window extends LogicTile implements Interactable {

	public static final BufferedImage ON_SPRITE = TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(4, 0, 1, 1),
			OFF_SPRITE = TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(3, 0, 1, 1);
	private int switchTime;

	public Window() {
		super(OFF_SPRITE, true);
	}

	@Override
	public void update() {
		if (switchTime < 15) {
			switchTime++;
		}
	}

	@Override
	public char id() {
		return 'w';
	}

	@Override
	public void use(Action actn) {
		if (actn.getName().equals("Use") && switchTime == 15) {
			switchTime = 0;
			if (getImage() == OFF_SPRITE) {
				setImage(ON_SPRITE);
			} else {
				setImage(OFF_SPRITE);
			}
		}
	}

}
