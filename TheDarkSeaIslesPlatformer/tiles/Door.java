package tiles;

import java.awt.image.BufferedImage;

import dayLite.grid.tiles.LogicTile;
import entities.Player;
import main.TheDarkSeaIslesPlatformer;

public class Door extends LogicTile {

	public static final BufferedImage CLOSED_SPRITE = TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(2, 0, 1, 1),
			OPENED_SPRITE = TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(1, 0, 1, 1);
	private boolean opened, unlocked;

	public Door() {
		super(CLOSED_SPRITE, true);
		opened = false;
		unlocked = true; // Keep for now
	}

	public void update() {

		if (unlocked && !opened && getLastEntityHit() instanceof Player) { // Door Unlocked, Isn't Already Opened, Player Hits The Door
			// Open The Door
			setImage(OPENED_SPRITE);
			setBlocked(false);
			opened = true;
		} else if (opened && getLastEntityHit() == null) { // Door Opened, But Player Is Off The Door 
			// Close The Door 
			setImage(CLOSED_SPRITE);
			setBlocked(true);
			opened = false;
		}
		setLastEntityHit(null); // Clear Last Entity To Touch Door, Entity Will Always Notify If Touching
	}

	public char id() {
		return 'D';
	}
}