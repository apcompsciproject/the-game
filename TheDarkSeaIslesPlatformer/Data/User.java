package Data;

import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Warning: You MUST create the user by getting it by name or Setting ALL fields
 * BEFORE saving it or you will recieve a NullPointerException
 */
public class User {
	private Connector c;
	private String name, state;
	private int health, stamina, attack, defense, x, y;

	public User() {
		c = new Connector();
	}

	/**
	 * @returns false if user does not exist;
	 */
	public boolean fetch(String name) {
		try {
			Object placeholder = c.getValue("name");
			if (placeholder == null)
				return false;

			update();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void update() {
		this.name = (String) c.getValue("name");
		this.state = (String) c.getValue("state");
		this.health = Integer.valueOf(c.getValue("health"));
		this.stamina = Integer.valueOf(c.getValue("stamina"));
		this.attack = Integer.valueOf(c.getValue("attack"));
		this.defense = Integer.valueOf(c.getValue("defense"));
		this.x = Integer.valueOf(c.getValue("x"));
		this.y = Integer.valueOf(c.getValue("y"));
	}

	public boolean save() {
		return !(!c.setValue("y", ((Integer) y).toString()) || !c.setValue("x", ((Integer) x).toString())
				|| !c.setValue("health", ((Integer) health).toString()) || !c.setValue("state", state)
				|| !c.setValue("name", name) || !c.setValue("stamina", ((Integer) stamina).toString())
				|| !c.setValue("attack", ((Integer) attack).toString())
				|| !c.setValue("defense", ((Integer) defense).toString()));
	}

	/**
	 * This helper method sets all int fields to 0 so that the user can be saved
	 * immediately. Reccomended only for new users.
	 */
	public void setAllToZero() {
		setHealth(0);
		setStamina(0);
		setAttack(0);
		setDefense(0);
	}

	/**
	 * This method adds a new user with the values as defined in the fields
	 * `fetch` is also called, to set the id, and check all values.
	 */
	public boolean saveAsNew() {
		ArrayList<String> f = new ArrayList<String>();
		f.add("name=" + name);
		f.add("health=" + health);
		f.add("stamina=" + stamina);
		f.add("attack=" + attack);
		f.add("defense=" + defense);
		f.add("state=" + state);
		f.add("x=" + x);
		f.add("y=" + y);
		String r = "";
		for (String s : f)
			r += s + '\n';
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(System.getProperty("user.home") + "/.the-game/save.dat");
			fileOut.write(r.getBytes());
			fileOut.close();
			fetch(name);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}

	public void setState(String s) {
		state = s;
	}

	public String getState() {
		return state;
	}

	public void setHealth(int h) {
		health = h;
	}

	public int getHealth() {
		return health;
	}

	public void setStamina(int s) {
		stamina = s;
	}

	public int getStamina() {
		return stamina;
	}

	public void setAttack(int a) {
		attack = a;
	}

	public int getAttack() {
		return attack;
	}

	public void setDefense(int d) {
		defense = d;
	}

	public int getDefense() {
		return defense;
	}

	public void setX(int ex) {
		x = ex;
	}

	public void setY(int why) {
		y = why;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}