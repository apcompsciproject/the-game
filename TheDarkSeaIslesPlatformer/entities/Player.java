package entities;

import Data.Setting;
import main.TheDarkSeaIslesPlatformer;
import status.actions.Interactable;
import java.awt.event.KeyEvent;
import dayLite.input.Key;
import dayLite.graphics.Animator;
import dayLite.graphics.ImageProcessor;
import dayLite.graphics.SpriteSheet;
import dayLite.grid.Camera;
import dayLite.grid.Grid;
import dayLite.grid.tiles.Tile;
import dayLite.input.InputHandler;
import status.actions.*;

public class Player extends Character {

	// Controls
	private InputHandler input;

	private Camera camera;

	boolean moving;
	private int direction;
	// 1
	// 2 4
	// 3
	private Key up, left, down, right, shift, use, punch;
	private static final int UP = 1, LEFT = 2, DOWN = 3, RIGHT = 4;
	private Animator movement;
	private double animSpeed;
	private static double ANIM_SPEED_WALKING = 0.10;
	private static double ANIM_SPEED_RUNNING = 0.05;
	

	public static final SpriteSheet PLAYER_SPRITES = new SpriteSheet(
			ImageProcessor.loadImage("sprites/player.png", false), 16);

	public Player(float x, float y, Grid g) {

		super(null, x, y, 0.4, 0.4, 0.2, g);

		// Animation And Images
		moving = false;
		animSpeed = ANIM_SPEED_WALKING;
		movement = new Animator(PLAYER_SPRITES.getSprite(0, 0, 12, 1), 16);
		this.setImage(PLAYER_SPRITES.getSprite(7, 0, 1, 1));

		// Camera
		camera = new Camera(3, 3, this.getGrid());

		// Controls
		input = new InputHandler(TheDarkSeaIslesPlatformer.GAME.getWindow());
		if (Setting.getSetting(1).equals("QWERTY")) {
			input.addKey(up = new Key(KeyEvent.VK_W));
			input.addKey(down = new Key(KeyEvent.VK_S));
			input.addKey(left = new Key(KeyEvent.VK_A));
			input.addKey(right = new Key(KeyEvent.VK_D));
			input.addKey(use = new Key(KeyEvent.VK_E));
			input.addKey(punch = new Key(KeyEvent.VK_J));
		} else {
			input.addKey(down = new Key(KeyEvent.VK_O));
			input.addKey(up = new Key(KeyEvent.VK_COMMA));
			input.addKey(left = new Key(KeyEvent.VK_A));
			input.addKey(right = new Key(KeyEvent.VK_E));
			input.addKey(use = new Key(KeyEvent.VK_W));
			input.addKey(punch = new Key(KeyEvent.VK_J));
		}
		input.addKey(shift = new Key(KeyEvent.VK_SHIFT));

	}

	public void update() {
		catchInput();

		if (moving) {
			this.setImage(movement.animate(animSpeed));
		} else {
			switch (direction) {
			case UP:
				this.setImage(PLAYER_SPRITES.getSprite(1, 0, 1, 1));
				break;
			case LEFT:
				this.setImage(PLAYER_SPRITES.getSprite(4, 0, 1, 1));
				break;
			case DOWN:
				this.setImage(PLAYER_SPRITES.getSprite(7, 0, 1, 1));
				break;
			case RIGHT:
				this.setImage(PLAYER_SPRITES.getSprite(10, 0, 1, 1));
				break;
			}
		}

		camera.center((getX() + (getWidth() / 2)), (getY() + (getHeight() / 2)));
	}

	public void catchInput() {
		double spd = getSpeed();
		animSpeed = ANIM_SPEED_WALKING;
		
		boolean dom_movement = false, sub_movement = false;

		// Set If We Are Moving
		moving = false;
		if (up.isPressed() || down.isPressed()) {
			dom_movement = true;
			moving = true;
			if (left.isPressed() || right.isPressed()) {
				sub_movement = true;
				moving = true;
			}
		}

		// Sprinting
		if (shift.isPressed()) {
			spd += .15;
			animSpeed = ANIM_SPEED_RUNNING;
		}

		// Fix Speed For Two Directions
		if (dom_movement && sub_movement) {
			spd /= Math.sqrt(2);
		}

		// Up
		if (up.isPressed()) {
			move(0, -spd);
			if (direction != UP) {
				movement.setLoop(0, 2);
			}
			direction = 1;
			moving = true;
		}

		// Down
		if (down.isPressed()) {
			move(0, spd);
			if (direction != DOWN) {
				movement.setLoop(6, 8);
			}
			direction = 3;
			moving = true;
		}

		// Left
		if (left.isPressed()) {
			move(-spd, 0);
			if (direction != LEFT && !dom_movement) {
				movement.setLoop(3, 5);
				direction = LEFT;
			}

			moving = true;
		}

		// Right
		if (right.isPressed()) {
			move(spd, 0);
			if (direction != RIGHT && !dom_movement) {
				movement.setLoop(9, 11);
				direction = RIGHT;
			}

			moving = true;
		}

		// Use Tiles
		if (use.isPressed()) {
			Tile tile = getTileInDirection();
			if (tile != null && tile instanceof Interactable) {
				((Interactable) tile).use(new Action("Use", 0));
			}
		}
		
		// Punch
		if (punch.isPressed()) {
			Tile tile = getTileInDirection();
			if (tile != null && tile instanceof Interactable) {
				((Interactable) tile).use(new Action("Punch", 10));
			}
		}

	}

	private Tile getTileInDirection() {
		double dist = 0.35;
		switch (direction) {
		case UP:
			return getGrid().getTile((int) (getX()), (int) (getY() - dist));
		case LEFT:
			return getGrid().getTile((int) (getX() - dist), (int) (getY()));
		case DOWN:
			return getGrid().getTile((int) (getX()), (int) (getY() + getHeight() + dist));
		case RIGHT:
			return getGrid().getTile((int) (getX() + getWidth() + dist), (int) (getY()));
		}
		return null;
	}

	public Camera getCamera() {
		return camera;
	}

	public String getName() {
		return "Player";
	}
}