package tiles;

import dayLite.grid.entities.Entity;
import dayLite.grid.tiles.LogicTile;
import entities.Player;

public class TrapStone extends LogicTile {
	
	public TrapStone() {
		super(Stone.STONE_SPRITE, false);
	}

	@Override
	public void update() {
		Entity hit = getLastEntityHit();
		if (hit instanceof Player) {
			setImage(null);
		} else if (getImage() != Stone.STONE_SPRITE) {
			setImage(Stone.STONE_SPRITE);
		}
		setLastEntityHit(null);
	}

	@Override
	public char id() {
		return 's';
	}

}
