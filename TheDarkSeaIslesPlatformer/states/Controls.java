package states;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import Data.Setting;
import dayLite.main.GameThread;
import dayLite.states.State;
import main.TheDarkSeaIslesPlatformer;

public class Controls extends State {

	MenuList m = new MenuList();
	private String priPrior;
	MenuItem b = new MenuItem(MenuItem.FULL, Setting.getSetting(1), 2);
	MenuItem contr = new MenuItem(MenuItem.FULL, "View Controls", 3);
	MenuItem back = new MenuItem(MenuItem.FULL, "Back", 4);
	

	
	public Controls(GameThread g, String c) {
		super(g);
		// TODO Auto-generated constructor stub
		b.addActionListener(kb);
		back.addActionListener(backScrAction);
		//contr.addActionListener();
		
		m.create(b);
		m.create(back);
		//m.create(contr);
		
		
		back.setDisabled(false);
		priPrior = c;
	}
	

	@SuppressWarnings("serial")
	Action kb = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			List<String> f = null;
			try {
				f = Files.readAllLines(Paths.get(System.getProperty("user.home") + "/.the-game/settings"),
						Charset.forName("UTF-8"));

				if (f.get(1).equals("QWERTY"))
					f.set(1, "DVORAK");
				else 
					f.set(1, "QWERTY");

				String r = "";
				for (String s : f)
					r += s + '\n';
				FileOutputStream fileOut = new FileOutputStream(
						System.getProperty("user.home") + "/.the-game/settings");
				fileOut.write(r.getBytes());
				fileOut.close();
				((MenuItem)e.getSource()).setText(Setting.getSetting(1));

			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
	};
	
/*	@SuppressWarnings("serial")
	Action controlImage = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.setState();
		}
	};*/
	
	@SuppressWarnings("serial")
	Action backScrAction = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			m.removeList();
			TheDarkSeaIslesPlatformer.GAME.setState(new Settings(TheDarkSeaIslesPlatformer.GAME, priPrior, "Controls"));
		}
	};
	
	public void render(Graphics2D g, double interpolation){
		g.setColor(new Color( Integer.parseInt(Setting.getSetting(2))));
		g.fillRect(0, 0, 1000, 1000);
		g.setColor(Color.RED);
		try {
			Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")).deriveFont(150f);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			//register the font
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/zapfino.ttf")));
			g.setFont(customFont);
		} catch (IOException | FontFormatException e) {
			e.printStackTrace();
			g.setFont(new Font("MS Gothic", Font.PLAIN, 80));
		}
		g.drawString("Controls", 230, 120);
		
		//TODO Scale and attach to an actual button...
		BufferedImage image;
		try {
			image = ImageIO.read(new File("sprites/keyboard.png"));
			g.drawImage(image, 0, 0, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		g.setColor(Color.gray);
		m.paint(g);
		
	}



	@Override
	public void update() {
	}
}
