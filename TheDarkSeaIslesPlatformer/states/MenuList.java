package states;

import java.awt.Graphics;
import java.util.ArrayList;

import main.TheDarkSeaIslesPlatformer;

@SuppressWarnings("serial")
public class MenuList extends ArrayList<MenuItem> {
	public void paint(Graphics g) {
		for (MenuItem i : this)
			i.paint(g);
	}

	public void create(MenuItem m) {
		add(m);
		m.setBounds();
		TheDarkSeaIslesPlatformer.GAME.add(m);
	}

	public void removeList() {
		for (MenuItem i : this)
			TheDarkSeaIslesPlatformer.GAME.remove(i);
	}
}