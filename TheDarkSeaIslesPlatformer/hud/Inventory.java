package hud;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import entities.Player;

/**
 * Displays inventory screen
 * 
 * @author Sam
 * @version 1
 */
public class Inventory extends HudItem
{
   
    public Inventory(Player p)
    {
        super(p);
    }

	@Override
	public void draw(Graphics2D g) {
		for (int n=0; n<5;n++)
		for(int i=0;i<5;i++)
			if(p.getInventory().get(n,i)!= null)
				g.drawImage(p.getInventory().get(n, i).getImage(), n*150+140+(i+1)*30, 570, 20, 20, null);
		
	}
    

}