package tiles;

import java.awt.image.BufferedImage;

import dayLite.grid.tiles.Tile;
import main.TheDarkSeaIslesPlatformer;

public class Stone extends Tile {
	
	public static final BufferedImage STONE_SPRITE = TheDarkSeaIslesPlatformer.TOWN_SPRITES.getSprite(0, 0, 1, 1);

	public Stone() {
		super(STONE_SPRITE, true);
	}

	public char id() {
		return 'S';
	}
}