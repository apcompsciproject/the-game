package status.actions;

public interface Interactable {

	public void use(Action actn);
}