package hud;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import Data.Setting;
import entities.Player;

public class Bar extends HudItem {
private Player player;
	public Bar(Player p) {
super(p);
		player=p;
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(new Color(Integer.parseInt(Setting.getSetting(2))));
		g.fillRect(150, 550, 500, 50);
		g.setColor(Color.red);
		for(int i=1; i<player.getHealth(); i++)
			g.fillOval(140+(i)*20, 555, 10, 10);
		
	}


}