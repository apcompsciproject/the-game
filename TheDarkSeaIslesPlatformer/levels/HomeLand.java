 package levels;

import java.util.ArrayList;

import entities.*;
import entities.items.collectables.MagicDust;
import dayLite.grid.entities.Entity;
import dayLite.levels.Level;
import dayLite.levels.LevelData;

public class HomeLand implements LevelData {

	private char[][] tiles;
	private ArrayList<Entity> entities;

	private Level level;
	private Player player;

	public HomeLand(Player p, Level lvl) {
		tiles = new char[][] {
				{ 'S', 'w', 'S', 'D', 'S', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T' },
				{ 'S', 'C', 'f', 'f', 'S', 'S', 'S', 'S', 'R', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'S', 'f', 'f', 'f', 'f', 'f', 'f', 's', ',', 'R', 'g', 't', 'g', 't', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'r', 'g', 'g', 'T' },
				{ 'S', 'f', 'f', 'f', 'f', 'f', 'f', 'S', 't', 'g', 'g', 'd', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'd', 'd', 'g', 'd', 'T' },
				{ 'S', 'f', 'f', 'f', 'S', 'S', 'w', 'S', 'R', 'g', 't', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 't', 'g', 'd', 'g', 'g', 'g', 'T' },
				{ 'S', 'f', 'f', 'f', 'S', 'd', 'g', 'g', 'g', 'g', 'g', 't', ',', 'g', 'd', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'S', 'w', 'S', 'D', 'S', ',', 'R', 'g', 'g', ',', 'g', 'g', 'g', 'd', 'g', 'g', 'R', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'T', 'g', 'g', 'g', 'g', 'g', 'g', 't', 'g', 'd', 'g', 'g', ',', 'g', 'g', 'g', ',', 't', ',', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'T', 'g', 'd', 'g', ',', 'g', 'g', 'g', 'd', 'g', 't', 'r', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'T', ',', 'd', 'v', 'g', ',', 'g', 'g', 'd', ',', 'g', 'g', 't', 'g', 'd', 'd', 't', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'T' },
				{ 'T', 'g', 'B', 'W', 'B', 'g', 'g', 'g', ',', 'g', 'g', ',', 'g', 'g', ',', 't', 'g', 'g', ',', 'g', 'r', 'g', 'g', 'g', 'T' },
				{ 'T', 'g', 'g', 'B', 'g', 'g', 'd', 'g', 'g', 't', 'g', 'g', 'd', 'd', 'g', 'g', 'g', 'g', 'g', 'd', 'd', 'g', 'g', 'd', 'T' },
				{ 'T', 'g', 'r', 'g', 't', ',', 'd', 'd', 'r', ',', 'g', 't', 'd', 'd', ',', 'g', 'g', 't', ',', 'g', 'g', 'g', 'g', 'd', 'T' },
				{ 'T', 'g', 'g', 'g', 'g', 'g', 'd', ',', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', ',', ',', 'g', 'g', 'g', 'd', 'd', 'T' },
				{ 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T' } };

		level = lvl;
		entities = new ArrayList<>();
		entities.add(p);
		player = p;

		entities.add(new MagicDust(2, 2, null));
	}

	public char[][] tileMemory() {
		return tiles;
	}

	public ArrayList<Entity> entityMemory() {
		return entities;
	}

	public Player getPlayer() {
		return player;
	}

	public void update() {
		if (player.getX() >= 3 && player.getX() <= 4 && player.getY() <= 0.25) {
			player.setPosition(1, 5);
			level.setLevel(new HomeInterior(player, level));
		}
	}
}